import Mince from './mince/mince.mjs';
import config from './mince/default_config.mjs';

addEventListener('DOMContentLoaded', (event) => {
  window.mince = new Mince(document.getElementById('mince'), { config });
  window.mince.el.focus();
});


import MText from './mtext.mjs';
import Globals from './globals.mjs';
import Events from './events.mjs';
import Display from './display.mjs';
import Selector from './selector.mjs';
import Editor from './editor.mjs';
import AsyncPaginator from './async_paginator.mjs';

export default class Mince {
  constructor(element, opt = {}) {
    this.el = element;
    this.globals = new Globals(this);
    this.events = new Events(this, { defaultConfig: opt.config });
    this.mtext = new MText();
    // FIXME sucks that the paginator is so coupled to the display
    this.display = new Display(this);
    // FIXME: selector should be an optional component for display.
    // in the same way that editing can be headless, so should selecting
    this.selector = new Selector(this);
    this.editor = new Editor(this); 
    let d = this.display;
    this.paginator = new AsyncPaginator(this, {
      afterEachPage: (pages, newIndex) => {
        if (newIndex < d.line + d.rowcount + d.overflow) d.fillRows(pages);
      },
      afterLastPage: (pages) => {
        d.scrollbar.update(d.line, d.rowcount, pages.length);
      }
    });
  }
}

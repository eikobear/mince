export default class AsyncPaginator {
  constructor(mince, opt = {}) {
    this.sample_el = mince.display.rows[0];
    this.el = mince.display.paginatorRow;

    // subtract 6 to ensure pagination doesn't overlap the scrollbar
    // when a page-break occurs exactly on a space.
    // FIXME: could this be fixd by skipping over the very last space,
    // as well as fixing lines with no spaces?`
    this.width = opt.width || this.sample_el.offsetWidth - 6;
    this.afterEachPage = opt.afterEachPage;
    this.afterLastPage = opt.afterLastPage;
    this.mtext = mince.mtext;
    this.pages = [this.mtext.slice(0, 0)];
    this.cachedLength = 0;
    this.paginating = true;
    this.lastIndex = 0;
    this.map = {};
    this.frameRequest = null;
    
    this.paginate();
  }

  cancel() {
    if (this.frameRequest) cancelAnimationFrame(this.frameRequest);
    this.frameRequest = null;
  }

  paginate(index = 0) {
    if (this._paginateFrame != null) return;

    this._paginateFrame = requestAnimationFrame(() => {
      this.cancel();
      let { line, column } = this.indexToPos(index);
      this.lastIndex = index - column;
      this.pages = this.pages.slice(0, line);
      this.paginating = true;
      this.asyncMakePages();
      this._paginateFrame = null;
    });
  }

  indexToPos(index) {
    if (index <= 0) {
      return { line: 0, column: 0 };
    }

    if (index >= this.cachedLength) {
      return { line: this.pages.length - 1, column: (this.pages[this.pages.length - 1].text || '').length };
    }

    let i = 0;
    for (i = 0; i < this.pages.length; i++) {
      index -= this.pages[i].length;

      // index < 0 places caret at start of line
      // index <= 0 places caret at end of previous line
      if (index < 0) break;
    }

    return { line: i, column: this.pages[i].length + index };
  }

  posToIndex(line, column) {
    line = Math.min(line, this.pages.length);
    let index = 0;
    for (let i = 0; i < line; i++) {
      index += this.pages[i].length;
    }

    return index + column;
  }

  /*asyncMakePages() {
    this.frameRequest = requestAnimationFrame(this.makePages.bind(this));
  }*/

  asyncMakePages() {
    let startLength = this.pages.length;
    this.makePage();
    this.afterEachPage(this.pages, startLength);
    if (this.paginating) {
      this.frameRequest = requestAnimationFrame(this.asyncMakePages.bind(this));
    }
  }

  makePage(timeout = 5, guarantee = 3) {
    let pagesMade = 0;
    let startTime = Date.now();
    let currentWidth = 0;
    let textLength = this.mtext.length;
    for (let i = this.lastIndex; i < textLength; i++) {
      let c = this.mtext.text[i];
      let zones = this.mtext._spacearray.find(i).zones;
      let charWidth = this.charWidth(c, zones);
      if (c === '\n') {
        currentWidth = 0;
        pagesMade++;
        this.pages.push(this.mtext.slice(this.lastIndex, i + 1));
        this.lastIndex = i + 1;

        if (pagesMade >= guarantee && Date.now() - startTime > timeout) return this.pages;
        continue;
      }

      if (currentWidth + charWidth > this.width) {
        let line = this.mtext.slice(this.lastIndex, i);
        //skip over last space to fix scrollbar issue mentioned above?
        //let spaceIndex = line.slice(0, -1).lastIndexOf(' ');
        let spaceIndex = line.text.lastIndexOf(' ');
        if (spaceIndex > -1) {
          line = line.slice(0, spaceIndex + 1);
          i = this.lastIndex + spaceIndex;
          this.lastIndex = i + 1;
        }
        else {
          this.lastIndex = i;
        }
        pagesMade++;
        this.pages.push(line);
        currentWidth = 0;
        if (pagesMade >= guarantee && Date.now() - startTime > timeout) return this.pages;
        continue;
      }

      currentWidth += charWidth;
    }

    let last = this.mtext.slice(this.lastIndex);
    this.pages.push(last || '');

    this.paginating = false;
    this.afterLastPage(this.pages);
    this.cachedLength = this.mtext.length;
    return this.pages;
  }

  textWidth(text) {
    let width = 0;
    for (let i = 0; i < text.length; i++) {
      width += this.charWidth(text[i]);
    }

    return width;
  }

  charWidth(character, zones, repeats = 20) {
    //charcode is about twice as fast as using the character
    let charcode = character.codePointAt(0);
    let classes = Object.keys(zones).map((k) => (`mz-${k}`)).join(' ');
    let val = (this.map[charcode] || {})[classes];
    if (val == null) {
      let text = character.repeat(repeats);
      let atomized = text.replace(/\t/g, `<span class='ma-tab'>\t</span>`);
      this.el.innerHTML = `<span class='${classes}'>${atomized}</span>`;
      this.map[charcode] = this.map[charcode] || {};
      val = this.map[charcode][classes] = this.el.firstChild.offsetWidth / repeats;
      this.el.innerHTML = '';
    }

    return val;
  }
}

import Puretext from './puretext.mjs';
import Spacearray from './spacearray.mjs';

export default class MText {

  constructor(text = '') {
    this._puretext = new Puretext(text);
    this._spacearray = new Spacearray(text.length);
  }

  insert(text, index = null) {
    this._puretext.insert(text, index);
    this._spacearray.insert(text.length, index);
  }

  remove(count, index = null) {
    this._puretext.remove(count, index);
    this._spacearray.remove(count, index);
  }

  slice(start, end) {
    let mt = new MText();
    mt._puretext = this._puretext.slice(start, end);
    mt._spacearray = this._spacearray.slice(start, end);
    return mt;
  }

  get text() {
    return this._puretext.text;
  }

  zonesAt(index) {
    return { ...this._spacearray.find(index).zones };
  }

  // TODO: this should go into spacearray
  markRange(start, end, zones) {
    if (start === end) return;
    if (end < start) {
      let t = start;
      start = end;
      end = t;
    }
    this._spacearray.mark(start);
    this._spacearray.mark(end, zones);
    let s = this._spacearray.find(start + 1);
    let e = this._spacearray.find(end);
    while (s != null && s !== e) {
      s.zones = { ...s.zones, ...zones };
      s = s.n;
    }
    this._spacearray.merge();
  }

  unmarkAll(zone) {
    this._spacearray.each((mark) => {
      delete mark.zones[zone];
    });
  }

  get length() {
    return this._puretext.length;
  }

}

export default class Regulator {
  constructor(opt = {}) {
    this.queued = {};
    this.commands = [];
    this.deliberators = {};
    this.frameRequest = null;
  }

  register(opt) {
    this.commands.push([opt.name, opt.callback]);
    this.deliberators[opt.name] = opt.deliberator;
  }

  queue(...args) {
    let [command, ...params] = args;
    let oldParams = this.queued[command];
    this.queued[command] = (oldParams == null) ? params : this.deliberators[command](oldParams, params);
    if (this.frameRequest == null) this.frameRequest = requestAnimationFrame(this.execute.bind(this));
  }

  execute() {
    let queue = this.queued;
    this.queued = {};
    this.commands.forEach((command) => {
      let [name, callback] = command;
      if (queue[name] == null) return;
      callback(...queue[name]);
    });
    this.frameRequest = null;
  }

}

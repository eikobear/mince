export default class Globals {
  constructor(mince) {
    this.mince = mince;
    document.addEventListener('mousemove', (event) => {
      this.mouseX = event.clientX;
      this.mouseY = event.clientY;
    });
  }
}

import Caret from './caret.mjs';

export default class Editor {
  constructor(mince, opt = {}) {
    this.mince = mince;
    this.area = mince.display;
    this.mtext = this.mince.mtext;
    this.el = this.mince.el;
    this.caret = new Caret(this.mince);
    this.index = 0;
    this.zoneState = {};

    this.el.addEventListener('focusin', (event) => {
      this.caret.show();
    });

    this.el.addEventListener('focusout', (event) => {
      this.caret.hide();
    });
  }

  // CLEANUP requires pages, causes linemove, statechange
  // FIXME instead of setTimeout, make caret wait for display ready
  setIndex(index) {
    let oldIndex = this.index;
    this.index = index;
    this.index = Math.max(0, Math.min(this.mtext._puretext.text.length, this.index));
    let { line } = this.area.indexToPos(this.index);
    this.area.moveToLine(line);
    setTimeout(() => {
      let { x, y } = this.area.indexToPx(this.index);
      this.caret.setLocation(x, y);
    }, 16);
    return this.index - oldIndex;
  }

  // CLEANUP requires pages, causes linemove, statechange
  moveIndex(amount) {
    return this.setIndex(this.index + amount);
  }

  queueZone(zone) {
    if (this.zoneState[zone] == null) {
      let current = this.mtext.zonesAt(this.index);
      this.zoneState[zone] = (current[zone]) ? 'off' : 'on';
    }
    else {
      this.zoneState[zone] = (this.zoneState[zone] === 'on') ? 'off' : 'on';
    }

    this.caret.applyZones(this.zoneState);
    return this.zoneState[zone];
  }

  // CLEANUP causes paginate
  applyZoneQueue(length) {
    if (Object.keys(this.zoneState).length === 0) return;

    this.mtext._spacearray.mark(this.index - length);
    let zones = { ...this.mtext.zonesAt(this.index) };
    Object.keys(this.zoneState).forEach((key) => {
      if (this.zoneState[key] === 'on')
        zones[key] = true;
      else
        delete zones[key];
    });
    this.mtext._spacearray.mark(this.index, zones, { replace: true });
    this.mince.paginator.paginate(this.index - length);
    this.zoneState = {};
  }

  // CLEANUP causes pagination, causes index move which requires pages
  insert(text) {
    this.mtext.insert(text, this.index);
    this.mince.paginator.paginate(this.index);
    this.moveIndex(text.length);
    this.mince.events.fire('afterinsert', event, text);
  }

  // CLEANUP causes paginate
  removeRange(start, end) {
    if (start === end) return;
    if (end < start) {
      let t = start;
      start = end;
      end = t;
    }

    this.mtext.remove(end - start, start);
    this.mince.paginator.paginate(start);
  }


}

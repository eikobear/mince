export default class Events {
  constructor(mince, opt = {}) {
    this.mince = mince;
    this.configs = [];
    this.addConfig(opt.defaultConfig || {});
    this.doubleclickSensitivity = opt.doubleclickSensitivity || 500;
    this.dragPollInterval = opt.dragPollInterval || 20;

    requestAnimationFrame(() => (this.buildDomEvents()));
  }

  addConfig(config) {
    let lowerConfig = {};
    Object.keys(config).forEach((key) => {
      let lowerKey = key.toLowerCase();
      lowerConfig[lowerKey] = config[key];
    });

    this.configs.unshift(lowerConfig);
  }

  fire(eventName, ...args) {
    this.configs.forEach((config) => {
      if (config[eventName] != null) config[eventName](this.mince, ...args);
    });
  }

  buildDomEvents() {
    document.addEventListener('mouseup', (event) => {
      clearInterval(this._dragInterval);
      this._dragInterval = null;
      this.fire('mouseup', event);
    });

    this.mince.el.addEventListener('mousedown', (event) => {
      let t = new Date();
      if (t - this._lastMouseDown > this.doubleclickSensitivity) this._clicks = 0;
      if (this._clicks >= 3) this._clicks = 0;
      this._clicks += 1;
      this._lastMouseDown = t;
      let { line, column } = this.mince.display.pxToPos(event.clientX, event.clientY);
      let index = this.mince.display.posToIndex(line, column);
      this._clickindex = index;
      this.fire('mousedown', event, index);
      if (this._clicks == 2) this.fire('doublemousedown', event, index);
      if (this._clicks == 3) this.fire('triplemousedown', event, index);

      if (this._dragInterval == null) {
        this._dragInterval = setInterval(() => {
          let { line, column } = this.mince.display.pxToPos(this.mince.globals.mouseX, this.mince.globals.mouseY);
          let index = this.mince.display.posToIndex(line, column);

          // this prevents drag from occurring until the mouse moves.
          if (index !== this._clickindex) { 
            this.fire('mousedrag', null, index);
            this._clickindex = null;
          }
        }, this.dragPollInterval);
      }
    });

    this.mince.el.addEventListener('wheel', (event) => {
      this.fire('scroll', event)
    });

    this.mince.el.addEventListener('keydown', (event) => {
      const callback_keys = [];
      if (event.ctrlKey) callback_keys.push('Control');
      if (event.altKey) callback_keys.push('Alt');

      // some characters aren't affected by Shift when the numpad is used
      const unaffectedByShift = event.key.length > 1 || ['/', '*', '-', '+'].includes(event.key);
      if (event.shiftKey && unaffectedByShift) callback_keys.push('Shift');

      // prevent Control, Alt or Shift from being added twice.
      if (!callback_keys.includes(event.key)) callback_keys.push(event.key);

      const callback = callback_keys.join(' ').toLowerCase();
      this.fire(callback, event);
    });

    this.mince.el.addEventListener('keypress', (event) => {
      let character = String.fromCharCode(event.charCode);
      if (event.key === 'Enter') character = '\n';
      if (event.charCode > 0 && character != null) this.fire('input', event, character);
    });
  }
}

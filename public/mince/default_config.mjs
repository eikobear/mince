export default {
  'Shift': (mince) => {
    if (mince.selector.anchor == null) {
      mince.selector.setAnchor(mince.editor.index);
    }
  },
  'MouseDown': (mince, event, index) => {
    mince.editor.setIndex(index);
    if (!event.shiftKey) {
      mince.selector.deselect();
      mince.selector.setAnchor(index);
    }
  },
  'DoubleMouseDown': (mince, event, index) => {
    let c = mince.mtext.text[index];
    let i = (c == null || c === '\n') ? index - 1 : index;
    let regex = (mince.mtext.text[i].match(/\S/) != null) ? /\s/ : /[\n\S]/;
    while (((c = mince.mtext.text[i]) != null) && c.match(regex) == null) i -= 1;
    let start = i + 1;
    i = index;
    while (((c = mince.mtext.text[i]) != null) && c.match(regex) == null) i += 1;
    let end = i;
    mince.editor.setIndex(end);
    mince.selector.setAnchor(start);
    mince.selector.setLimit(end);
  },
  'TripleMouseDown': (mince, event, index) => {
    let c = mince.mtext.text[index];
    let i = (c == null || c === '\n') ? index - 1 : index;
    while(((c = mince.mtext.text[i]) != null) && c.match(/^[\n]/) == null) i -= 1;
    let start = i + 1;
    i = index;
    while(((c = mince.mtext.text[i]) != null) && c.match(/^[\n]/) == null) i += 1;
    let end = i;
    mince.editor.setIndex(end);
    mince.selector.setAnchor(start);
    mince.selector.setLimit(end);
  },
  'MouseDrag': (mince, event, index) => {
    mince.editor.setIndex(index);
    mince.selector.setLimit(index);
  },
  'Input': (mince, event, text) => {
    let range = mince.selector.range;
    if (!range.includes(null)) {
      mince.editor.removeRange(...range);
      mince.editor.setIndex(range[0]);
    }
    mince.selector.deselect();
    mince.editor.insert(text);
  },
  'AfterInsert': (mince, event, text) => {
    mince.editor.applyZoneQueue(text.length);
  },
  'Control b': (mince, event) => {
    mince.editor.queueZone('bold');
    event.preventDefault();
    event.stopPropagation();
  },
  'Control i': (mince, event) => {
    mince.editor.queueZone('italic');
    event.preventDefault();
    event.stopPropagation();
  },
  'Control u': (mince, event) => {
    mince.editor.queueZone('underline');
    event.preventDefault();
    event.stopPropagation();
  },
  'Control s': (mince, event) => {
    mince.editor.queueZone('strikethrough');
    event.preventDefault();
    event.stopPropagation();
  },
  'PageDown': (mince) => {
    mince.moveLine(mince.rowcount - 1);
  },
  'PageUp': (mince) => {
    mince.moveLine(-(mince.rowcount - 1));
  },
  'Control ArrowDown': (mince) => {
    mince.moveLine(mince.rowcount - 1);
  },
  'Control ArrowUp': (mince) => {
    mince.moveLine(-(mince.rowcount - 1));
  },
  'Tab': (mince, event) => {
    event.preventDefault();
    event.stopPropagation();
    return '\t';
  },
  'Backspace': (mince, event) => {
    event.preventDefault();
    event.stopPropagation();
    let range = mince.selector.range;
    if (range.includes(null)) { 
      mince.editor.removeRange(mince.editor.index, Math.max(0, mince.editor.index - 1));
      mince.editor.moveIndex(-1);
      return;
    }

    mince.editor.removeRange(...range);
    mince.editor.setIndex(range[0]);
    mince.selector.deselect();
  },
  'Delete': (mince, event) => {
    event.preventDefault();
    event.stopPropagation();
    let range = mince.selector.range;
    if (range.includes(null)) { 
      mince.editor.removeRange(mince.editor.index, mince.editor.index + 1);
      return;
    }

    mince.editor.removeRange(...range);
    mince.editor.setIndex(range[0]);
    mince.selector.deselect();
  },
  'ArrowUp': (mince) => {
    let { line, column } = mince.display.indexToPos(mince.editor.index);
    let index;
    if (line <= 0) {
      index = 0;
    } else {
      column = Math.min(column, (mince.paginator.pages[line - 1].text.replace(/\n$/, '') || '').length);
      index = mince.display.posToIndex(line - 1, column);
    }

    mince.editor.setIndex(index);
    mince.selector.deselect();
  },
  'Shift ArrowUp': (mince) => {
    let { line, column } = mince.display.indexToPos(mince.editor.index);
    let index;
    if (line <= 0) {
      index = 0;
    } else {
      column = Math.min(column, (mince.paginator.pages[line - 1].text.replace(/\n$/, '') || '').length);
      index = mince.display.posToIndex(line - 1, column);
    }

    mince.editor.setIndex(index);
    mince.selector.setLimit(index);
  },
  'ArrowDown': (mince) => {
    let { line, column } = mince.display.indexToPos(mince.editor.index);
    let index;
    if (line >= mince.paginator.pages.length - 1) {
      index = mince.mtext.text.length;
    } else {
      column = Math.min(column, (mince.paginator.pages[line + 1].text.replace(/\n$/, '') || '').length);
      index = mince.display.posToIndex(line + 1, column);
    }

    mince.editor.setIndex(index);
    mince.selector.deselect();
  },
  'Shift ArrowDown': (mince) => {
    let { line, column } = mince.display.indexToPos(mince.editor.index);
    let index;
    if (line >= mince.paginator.pages.length - 1) {
      index = mince.mtext.text.length;
    } else {
      column = Math.min(column, (mince.paginator.pages[line + 1].text.replace(/\n$/, '') || '').length);
      index = mince.display.posToIndex(line + 1, column);
    }

    mince.editor.setIndex(index);
    mince.selector.setLimit(index);
  },
  'ArrowLeft': (mince) => {
    mince.editor.moveIndex(-1);
    mince.selector.deselect();
  },
  'Shift ArrowLeft': (mince) => {
    mince.editor.moveIndex(-1);
    mince.selector.setLimit(mince.editor.index);
  },
  'ArrowRight': (mince) => {
    mince.editor.moveIndex(1);
    mince.selector.deselect();
  },
  'Shift ArrowRight': (mince) => {
    mince.editor.moveIndex(1);
    mince.selector.setLimit(mince.editor.index);
  },
}


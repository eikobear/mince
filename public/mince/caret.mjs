export default class Caret {
  constructor(mince) {
    this.mince = mince;
    this.el = document.createElement('span');
    this.chars = { light: '▏', heavy: '▎'}
    this.el.appendChild(document.createTextNode(this.chars.light));
    this.el.className = 'mince caret mince-caret';
    this.mince.el.appendChild(this.el);
    this.hide();
    requestAnimationFrame(() => (this.setLocation(0, 0)));
  }

  applyZones(zones = {}, atPosition = true) {
    let currentZones = (atPosition) ? this.mince.mtext.zonesAt(this.mince.editor.index) : {};
    Object.keys(zones).forEach((key) => {
      if (zones[key] === 'on')
        currentZones[key] = true;
      else
        delete currentZones[key];
    });
    delete currentZones['selection'];
    let classes = Object.keys(currentZones).map((z) => (`mz-${z}`)).join(' ');
    this.el.className = `mince caret mince-caret ${classes}`;
    if (currentZones.bold) 
      this.setMode('heavy');
    else
      this.setMode('light');

    return currentZones;
  }

  setMode(mode) {
    this.el.textContent = this.chars[mode];
  }

  get visible() {
    return !this._hidden;
  }

  get hidden() {
    return this._hidden;
  }

  hide() {
    this.setVisible(false);
  }

  show() {
    this.setVisible(true);
  }

  setVisible(visible) {
    this._hidden = !visible;
    this.el.style.visibility = (visible) ? '' : 'hidden';
  }

  setLocation(x, y) {
    let rect = this.mince.display.el_box.getBoundingClientRect();
    this.el.style.left = `${rect.x + x}px`;
    this.el.style.top = `${rect.y + y}px`;
    this.applyZones();
  }
}


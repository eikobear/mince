export default class Scrollbar {

  constructor(mince) {
    this.mince = mince;
    this._hidden = false;
    this.el = document.createElement('div');
    this.el.className = 'mince scrollbar mince-scrollbar';
    this.el_bar = document.createElement('div');
    this.el_bar.className = 'bar';
    this.el.appendChild(this.el_bar);
    this.mince.el.appendChild(this.el);

    document.addEventListener('mouseup', (event) => {
      clearInterval(this.scrollKnobDragUpdate);
    });

    this.el.addEventListener('mousedown', (event) => {
      this.scrollKnobDragUpdate = setInterval( () => {
        let offsetY = this.mince.globals.mouseY - this.el.offsetTop;
        let availableHeight = this.el.offsetHeight - this.el_bar.offsetHeight;
        offsetY = Math.max(0, Math.min(availableHeight, offsetY));
        let maxLine = this.mince.paginator.pages.length - this.mince.display.rowcount;
        let newLine = Math.floor((offsetY / availableHeight) * maxLine);
        this.mince.display.setLine(newLine);
      }, 20);
    });
  }

  get hidden() {
    return this._hidden;
  }

  get visible() {
    return !this._hidden;
  }

  show() {
    this.setVisibility(true);
  }

  hide() {
    this.setVisibility(false);
  }

  setVisibility(visible) {
    this._hidden = !visible;
    this.el.style.visibility = (visible) ? '' : 'hidden';
  }

  update(currentLine, visibleLines, totalLines) {
    let height = Math.min(100, Math.max(1, (visibleLines / totalLines) * 100.0));
    let lowestPosition = totalLines - visibleLines;
    let offsetRatio = currentLine / lowestPosition;
    let offset = ((100.0 - height) * offsetRatio);
    this.el_bar.style.height = `${height}%`;
    this.el_bar.style.top = `${offset}%`;
    if (height === 100) {
      this.hide();
    }
    else {
      this.show();
    }
  }

}

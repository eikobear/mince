export default class Selector {

  constructor(mince, opt = {}) {
    this.mince = mince;
    this.anchor = null;
    this.limit = null;
    this.oldRange = [];
  }

  setAnchor(index) {
    if (index === this.anchor) return;

    this.oldRange = this.range;
    this.anchor = index;
    this.update();
  }

  setLimit(index) {
    if (index === this.limit) return;

    this.oldRange = this.range;
    this.limit = index;
    this.update();
  }

  get range() {
    return [this.anchor, this.limit].sort((a,b) => (a-b));
  }

  //FIXME: we shouldn't mark mtext with meta-data like selection.
  //maybe mincearea can have its own spacearray for selections.
  update() {
    let earliest = [...this.oldRange, ...this.range].filter((a) => (a != null)).sort((a,b) => (a-b))[0];
    if (earliest == null) return; 

    this.mince.mtext.unmarkAll('selection');
    if (!this.range.includes(null)) {
    this.mince.mtext.markRange(...this.range, { selection: true });
    }

    this.mince.paginator.paginate(earliest);
  }

  deselect() {
    this.oldRange = this.range;
    this.anchor = null;
    this.limit = null;
    this.update();
  }

}

export default class Paginator {

  constructor(mtext, test_element) {
    this.el = test_element;
    this.mtext = mtext;
    this.map = {};
  }

  fastPaginate(text, width = null) {
    width = width || this.el.offsetWidth;
    const paginated = [];

    let currentWidth = 0;
    let lastIndex = 0;
    let textLength = text.length;
    for (let i = 0; i < textLength; i++) {
      let c = text[i];
      let charWidth = this.charWidth(c);
      if (c === '\n') {
        currentWidth = 0;
        paginated.push(text.slice(lastIndex, i + 1));
        lastIndex = i + 1;
        continue;
      }

      if (currentWidth + charWidth > width) {
        currentWidth = 0;
        let line = text.slice(lastIndex, i + 1);
        let spaceIndex = line.lastIndexOf(' ');
        if (spaceIndex > -1) {
          line = line.slice(0, spaceIndex + 1);
          i = lastIndex + spaceIndex;
          lastIndex = lastIndex + spaceIndex + 1;
        }
        else {
          lastIndex = i + 1;
        }
        paginated.push(line);
        continue;
      }

      currentWidth += charWidth;
    }
    let last = text.slice(lastIndex);
    if (last && last !== '') paginated.push(last);
    return paginated;
  }

  textWidth(text) {
    let width = 0;
    for (let i = 0; i < text.length; i++) {
      width += charWidth(text[i]);
    }

    return width;
  }

  charWidth(character, repeats = 20) {
    //charcode is about twice as fast as using the character
    let charcode = character.codePointAt(0);
    let val = this.map[charcode];
    if (val == null) {
      this.el.innerHTML = `<span>${character.repeat(repeats)}</span>`;
      val = this.map[charcode] = this.el.firstChild.offsetWidth / repeats;
    }

    return val;
  }

}

class Pages {
  constructor(pages, test_element) {

  }
}

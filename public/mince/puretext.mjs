export default class Puretext {

  constructor(text = '') {
    this._text = text;
  }

  slice(start, end) {
    let text = this._text.slice(start, end);
    return new Puretext(text);
  }

  insert(text, index = null) {
    if (index == null) index = this.length;
    if (index < 0 || index > this.length) throw `index ${index} is out of bounds`;

    const before = this._text.slice(0, index);
    const after = this._text.slice(index);
    this._text = before + text + after;
    return this;
  }

  remove(count, index = null) {
    if (index == null) index = this.length - count;
    if (index < 0 || index > this.length) throw `index ${index} is out of bounds`;

    const before = this._text.slice(0, index);
    const after = this._text.slice(index + count);
    const removed = this._text.slice(index, index + count);
    this._text = before + after;
    return this;
  }

  get text() {
    return this._text;
  }

  get length() {
    return this._text.length;
  }

}

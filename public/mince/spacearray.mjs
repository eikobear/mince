export default class Spacearray {

  constructor(space = 0) {
    this._head = new Mark(space);
  }

  insert(space, index = null) {
    if (index == null) index = this.length;
    if (index < 0 || index > this.length) throw `index ${index} is out of bounds`;

    let mark = this.at(index, (mark, offset) => (mark));
    if (mark == null) throw `something went wrong and mark could not be found`;

    mark.space += space;
    return this
  }

  slice(start, end) {
    let sa = new Spacearray();
    let last = sa._head;
    this.range(start, end, (mark) => {
      last.n = mark;
      mark.p = last;
      last = mark;
    });

    return sa;
  }

  range(start, end, callback) {
    let mark = this._head;
    let cumulative = 0;
    let first = true;
    let last_m = null;
    while (mark != null) {
      cumulative += mark.space;
      if (cumulative < start) { 
        mark = mark.n;
        continue;
      }
      let m = new Mark(mark.space, { ...mark.zones });
      m.p = last_m;
      if (last_m != null) last_m.n = m;
      
      if (first) { 
        m.space = cumulative - start;
        first = false;
      }

      if (cumulative > end) {
        m.space = m.space - (cumulative - end);
        callback(m);
        break;
      };
      
      callback(m);
      mark = mark.n;
      last_m = m;
    }
  }

  each(callback) {
    let mark = this._head;
    let cumulative = 0;
    while (mark != null) {
      cumulative += mark.space;
      callback(mark, cumulative);
      mark = mark.n;
    }

    return this;
  }

  all() {
    let marks = [];
    this.each((mark) => (marks.push(mark)));
    return marks;
  }

  //FIXME: this should be find, and find should merge with at
  findby(callback) {
    let mark = this._head;
    let cumulative = 0;
    while (mark != null) {
      cumulative += mark.space;
      if (callback(mark, cumulative)) return { mark: mark, cum: cumulative };
      mark = mark.n;
    }

    return null;
  }

  find(index) {
    let mark = this._head;
    let cumulative = 0;
    while (mark != null) {
      cumulative += mark.space;
      if (cumulative >= index) break;

      mark = mark.n;
    }

    return mark;
  }

  at(index, callback) {
    let mark = this._head;
    let cumulative = 0;
    while (mark != null) {
      cumulative += mark.space;
      if (cumulative >= index) break;
      
      mark = mark.n;
    }

    return callback(mark, cumulative - index);
  }

  mark(index, zones, opt = {}) {
    if (index < 0 || index > this.length) throw `index ${index} is out of bounds`;

    return this.at(index, (mark, offset) => {
      let newzones = (opt.replace) ? zones : { ...mark.zones, ...zones };
      if (mark.space - offset === 0) {
        mark.zones = newzones;
        return mark;
      }

      let newmark = new Mark(mark.space - offset, newzones);
      mark.space = offset;
      if (mark.space === 0) {
        newmark.n = mark.n;
        newmark.p = mark.p;
        if (newmark.p != null) newmark.p.n = newmark;
        if (newmark.n != null) newmark.n.p = newmark;
        if (newmark.p == null) this._head = newmark;
      }
      else {
        newmark.p = mark.p;
        newmark.n = mark;
        if (newmark.p != null) newmark.p.n = newmark;
        if (newmark.n != null) newmark.n.p = newmark;
        if (newmark.p == null) this._head = newmark;
      }

      return newmark;
    });
  }

  merge() {
    this.each((mark) => {
      if (mark.n != null && (mark.space === 0 || this.areArraysEqualSets(Object.keys(mark.zones), Object.keys(mark.n.zones)))) {
        mark.n.space += mark.space;
        mark.n.p = mark.p;
        if (mark.p != null)  {
          mark.p.n = mark.n;
        }
        else {
          this._head = mark.n;
        }
      }
    });
  }

  areArraysEqualSets(a1, a2) {
    let superSet = {};
    for (let i = 0; i < a1.length; i++) {
      const e = a1[i] + typeof a1[i];
      superSet[e] = 1;
    }

    for (let i = 0; i < a2.length; i++) {
      const e = a2[i] + typeof a2[i];
      if (!superSet[e]) {
        return false;
      }
      superSet[e] = 2;
    }

    for (let e in superSet) {
      if (superSet[e] === 1) {
        return false;
      }
    }

    return true;
  }


  remove(space, index = null) {
    if (index == null) index = this.length - space;
    if (index < 0 || index > this.length) throw `index ${index} is out of bounds`;

    this.at(index, (mark, offset) => {
      let remaining = space - offset;
      mark.space -= offset;
      if (remaining < 0) mark.space += -remaining;

      while(mark.n != null && remaining > 0) {
        mark = mark.n;
        remaining -= mark.space;
        mark.space = Math.max(-remaining, 0);
      }
    });

    this.merge();
    return this;
  }

  get length() {
    return this.all().reduce((sum, mark) => (sum + mark.space), 0);
  }

}

class Mark {

  constructor(space = 0, zones = {}) {
    this.space = space;
    this.zones = zones;
    this.p = null;
    this.n = null;
  }

  pluck() {
    if (this.p) this.p.n = this.n;
    if (this.n) this.n.p = this.p;
    return this;
  }
}

import Scrollbar from './scrollbar.mjs';

export default class Display {
  constructor(mince, opt = {}) {
    this.mince = mince;
    this.el = mince.el;
    this.el.className = `${this.el.className} mince display mince-display`;
    this.line = 0;
    this.el_pane = document.createElement('div');
    this.el_pane.className = 'mince pane mince-pane';
    this.el.appendChild(this.el_pane);
    this.el_box = document.createElement('div');
    this.el_box.className = 'mince box mince-box';
    this.el_pane.appendChild(this.el_box);
    this.scrollbar = new Scrollbar(this.mince);
    this.overflow = 10;
    this.buildStyle();
    this.buildRows();

    this.mince.events.addConfig({
      'scroll': (mince, event) => {
        let x = this.el_box.offsetLeft;
        let y = this.el_box.offsetTop + event.deltaY;
        let { line } = this.pxToPos(x, y);
        this.setLine(line);
      }
    });
  }

  buildStyle() {
    this.style = {};
    this.style.lineheight = Number(getComputedStyle(this.el).lineHeight.slice(0,-2));
  }

  // CLEANUP requires pages, causes fillrow, causes scrollbar update
  moveLine(line) {
    this.setLine(this.line + line);
  }

  // CLEANUP requires pages, causes fillrow, causes scrollbar update
  moveToLine(line) {
    if (line < this.line) return this.setLine(line);

    if (line >= this.line + this.rowcount) {
      let diff = line - (this.line + this.rowcount - 1);
      return this.moveLine(diff);
    }
  }

  // CLEANUP requires pages, causes fillrow, causes scrollbar update
  setLine(line) {
    this.line = Math.max(0, Math.min(this.mince.paginator.pages.length - this.rowcount, line));
    // animationframe allows setLine to be called safely on mouse events
    // otherwise focus is lost when elements change
    //requestAnimationFrame(() => {
    this.fillRows(this.mince.paginator.pages);
    this.scrollbar.update(this.line, this.rowcount, this.mince.paginator.pages.length)
    //});
  }

  // CLEANUP requires pages
  indexToPx(index, pages = null) {
    pages = pages || this.mince.paginator.pages;
    let { line, column } = this.indexToPos(index, pages);
    return this.posToPx(line, column, pages);
  }

  // CLEANUP requires pages
  indexToPos(index) {
    return this.mince.paginator.indexToPos(index);
  }

  // CLEANUP requires pages
  posToIndex(line, column) {
    return this.mince.paginator.posToIndex(line, column);
  }

  // CLEANUP requires pages
  pxToPos(clientX, clientY, pages = null) {
    pages = pages || this.mince.paginator.pages;
    let rect = this.el_box.getBoundingClientRect();
    let x = clientX - rect.left;
    let y = clientY - rect.top;
    // keep y positive, but move upward if it was negative
    let direction = (y > 0) ? 1 : -1;
    y *= direction;
    let line = -direction;
    while (y >= 0) {
      line += direction;
      y -= this.rows[this.overflow + line].getBoundingClientRect().height;
    }
    line += this.line;
    if (line < 0) return { line: 0, column: 0 };

    if (line >= pages.length) {
      line = pages.length - 1;
      let column = (pages[pages.length - 1].text || '').replace(/\n$/, '').length;
      return { line, column };
    }

    let text = pages[line].text;
    text = text.replace(/\n$/, '');
    let charWidth = 0;
    let column = 0;
    for (column = 0; column < text.length; column++) {
      let width = this.mince.paginator.charWidth(text[column], pages[line].zonesAt(column));
      charWidth += (width / 2.0);
      if (charWidth > x) break;

      charWidth += (width / 2.0);
    }

    return { line, column };
  }

  // CLEANUP requires pages
  posToPx(line, column, pages) {
    pages = pages || this.mince.paginator.pages;
    let text = pages[line].text;
    let lineOffset = line - this.line;
    let y = 0;
    for (let i = 0; i < lineOffset; i++) {
      y += this.rows[this.overflow + i].getBoundingClientRect().height;
    }
    let x = 0;
    for (let i = 0; i < column; i++) {
      x += this.mince.paginator.charWidth(text[i], pages[line].zonesAt(i));
    }

    return { x, y };
  }

  fillRows(pages, start = null) {
    if (this._fillRowsFrame != null) return;

    this._fillRowsFrame = requestAnimationFrame(() => {
      if (start == null) start = this.line - this.overflow;
      for(let i = 0; i < this.rowcountTotal; i++) {
        let page = pages[i + start];
        if (page != null)  {
          let innerHTML = '';
          page._spacearray.each((mark, cumulative) => {
            let slice = page.slice(cumulative - mark.space, cumulative);
            let classes = 'mz ';
            classes += Object.keys(mark.zones).map((k) => (`mz-${k}`)).join(' ');
            let atomized = slice.text.replace(/\t/g, `<span class='ma-tab'>\t</span>`);
            innerHTML += `<span class='${classes}'>${ atomized }</span>`;
          });
          this.rows[i].innerHTML = innerHTML;
        }
        else {
          this.rows[i].innerHTML = '';
        }
      }
      this._fillRowsFrame = null;
    });
  }

  buildRows() {
    if (this.el_rows != null) this.el_rows.remove();
    this.el_rows = document.createElement('div');
    this.el_rows.className = 'mince rows mince-rows';
    this.el_rows.style.top = `-${this.style.lineheight * this.overflow}px`;
    this.el_box.appendChild(this.el_rows);
    let fontSize = Number(getComputedStyle(this.el).fontSize.slice(0,-2));
    this.rowcount = Math.floor(this.el_box.clientHeight / this.style.lineheight);
    this.rowcountTotal = this.rowcount + (this.overflow * 2);
    this.rows = Array(this.rowcountTotal);
    for (let i = 0; i < this.rowcountTotal; i++) {
      let row = document.createElement('div');
      row.className = 'mince row mince-row';
      row.style.lineHeight = `${this.style.lineheight}px`;
      row.style.height = `${this.style.lineheight}px`;
      this.el_rows.appendChild(row);
      this.rows[i] = row;
    }

    // build test row for paginator to calculate charWidth
    this.paginatorRow = document.createElement('div');
    this.paginatorRow.className = 'mince row mince-row mince-paginator-row';
    this.el_rows.appendChild(this.paginatorRow);
  }

}


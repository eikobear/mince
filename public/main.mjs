import * as Mince from 'mince/mince.mjs';
import config from 'mince/default_config.mjs';

addEventListener('DOMContentLoaded', (event) => {
  window.mince = new Mince.MinceArea(document.getElementById('mince'), { config });
  document.querySelector('textarea').value = window.mince.mtext._puretext.text;
});


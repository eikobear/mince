
export default class Squabblebrash {

  static generate(words = 100) {
    let verbs = ['crambled ', 'sprinned ', 'swalted '];
    let nouns = ['poro-poro', 'daranji', 'speck', 'grambler'];
    let singulars = ['a ', 'the ', 'at least one '];
    let plurals = ['a few ', 'several ', 'some ', 'the '];
    let transitions = ['Then, ', 'Afterward, ', 'Eventually, ', 'One after another, '];
    let subjects = ['I ', 'they '];
    let ends = [...Array(20).fill('. '), ...Array(6).fill('! '), '!\n', '.\n', '.\n', '.\n\n', '!\n\n***\n\n'];
    let sb = ['Squabble ', 'and ', 'brash, ', 'I '];

    while (sb.length < words) {
      sb.push(this.randomItem(verbs));
      let article = this.randomItem(this.randomItem([singulars, plurals]));
      sb.push(article);
      let noun = this.randomItem(nouns);
      if (plurals.includes(article)) noun = noun.concat('s');
      sb.push(noun.concat(this.randomItem(ends)));
      sb.push(this.randomItem(transitions));
      sb.push(this.randomItem(subjects));
    }

    return sb.slice(0, words).join('');
  }

  static randomItem(array) {
    return array[Math.floor(Math.random() * array.length)];
  }

}

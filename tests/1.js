
let a = Array(30000000);


for(let i = 0; i < 1000; i++) {
  let index = Math.floor(Math.random() * 30000000);
  a[index] = i;
}

let trials = 10;
let total = 0;

for(let i = 0; i < trials; i++) {
  let t1 = Date.now();

  a.forEach((i) => {
    return i + 1
  });

  total += Date.now() - t1;
}

console.log(total / trials);


import Spacearray from '../../spacearray.mjs';

describe('puretext', () => {
  it('initializes with a default length of 0', () => {
    let s = new Spacearray();
    expect(s.length).to.equal(0);
  });

  it('can be initialized to any length', () => {
    let length = 4;
    let s = new Spacearray(length);
    expect(s.length).to.equal(length);
  });

  it('partitions existing space when marks are added', () => {
    let length = 10;
    let s = new Spacearray(length);
    let index = 4;
    let mark = s.mark(index);
    expect(s.all().length).to.equal(2);
    expect(mark.space).to.equal(index);
    expect(mark.n.space).to.equal(length - index);
    expect(s.length).to.equal(length);
  });

  it('grows the proper marker when space is inserted', () => {
    let length = 10;
    let s = new Spacearray(length);
    let index = 4;
    let mark = s.mark(index);
    let amount = 3;
    s.insert(amount, index - 1);
    expect(mark.space).to.equal(index + amount);
    expect(mark.n.space).to.equal(length - index);
    expect(s.length).to.equal(length + amount);
  });

  it('shrinks the proper marker when space is removed', () => {
    let length = 10;
    let s = new Spacearray(length);
    let index = 4;
    let mark = s.mark(index);
    let amount = 3;
    s.remove(amount, index - 1);
    expect(mark.space).to.equal(index - 1);
    expect(mark.n.space).to.equal((length - index) - 2);
    expect(s.length).to.equal(length - amount);
  });

});


import Puretext from '../../puretext.mjs';

describe('puretext', () => {
  it('initializes with an empty string', () => {
    let p = new Puretext();
    expect(p.text).to.equal('');
  });

  it('can be initialized with a default string', () => {
    let s = 'foo'
    let p = new Puretext(s);
    expect(p.text).to.equal(s);
  });

  it('returns the length of the string', () => {
    let s = 'foo'
    let p = new Puretext(s);
    expect(p.length).to.equal(s.length);
  });

  it('appends text when inserted without an index', () => {
    let s = 'foo'
    let s2 = 'bar'
    let p = new Puretext(s);
    p.insert(s2);
    expect(p.text).to.equal(`${s}${s2}`);
  });

  it('inserts text at the specified index', () => {
    let s = 'foo'
    let s2 = 'bar'
    let p = new Puretext(s);
    p.insert(s2, 1);
    expect(p.text).to.equal('fbaroo');
  });

  it('truncates text when removed without an index', () => {
    let s = 'foobar';
    let p = new Puretext(s);
    p.remove(3);
    expect(p.text).to.equal(s.slice(0, 3));
  });

  it('removes text at the specified index', () => {
    let s = 'foobar'
    let p = new Puretext(s);
    p.remove(3, 2);
    expect(p.text).to.equal('for');
  });

  it('errors when index is less than 0', () => {
    let p = new Puretext();
    let index = -1;
    expect(() => (p.insert('x', index))).to.throw(`index ${index} is out of bounds`);
    expect(() => (p.remove(1, index))).to.throw(`index ${index} is out of bounds`);
  });

  it('errors when index is greater than length', () => {
    let s = 'foo'
    let p = new Puretext(s);
    let index = s.length + 1;
    expect(() => (p.insert('x', index))).to.throw(`index ${index} is out of bounds`);
    expect(() => (p.remove(1, index))).to.throw(`index ${index} is out of bounds`);
  });

  it('returns self after calls', () => {
    let p = new Puretext();
    let value = p.insert('anything');
    expect(value).to.equal(p);
    value = p.remove(1);
    expect(value).to.equal(p);
  });

});

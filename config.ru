require 'roda'

Rack::Mime::MIME_TYPES.merge!({ '.mjs' => 'application/javascript' })

class App < Roda
  plugin :public

  route do |r|
    r.on 'test' do
      'hello'
    end
    r.public
  end

end

run App.freeze.app
